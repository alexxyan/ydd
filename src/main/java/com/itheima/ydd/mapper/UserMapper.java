package com.itheima.ydd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.ydd.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2022-02-18
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


}
