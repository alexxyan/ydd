package com.itheima.ydd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.ydd.entity.Role;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-22
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}
