package com.itheima.ydd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.ydd.entity.Config;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-21
 */
@Mapper
public interface ConfigMapper extends BaseMapper<Config> {

}
