package com.itheima.ydd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.ydd.entity.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-22
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {


    @Select(" <script>" +
            "SELECT DISTINCT m.* FROM sys_menu m, sys_role_menu rm WHERE m.id = rm.menu_id and  rm.role_id  in " +
            " <foreach collection='strings' open='(' item='id' separator=',' close=')'> #{id}</foreach> AND m.type = 1" +
            " </script>")
    List<Menu> findMenuList(@RequestParam("strings") String[] strings);


}
