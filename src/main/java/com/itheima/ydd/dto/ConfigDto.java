package com.itheima.ydd.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class ConfigDto implements Serializable {
    private Integer id;
    private String attr_key;
    private String attr_value;
    private String attr_name;
    private List<Map<String, String>> config_value;
    private String group_value;
    private String placeholder;
    private Integer sort_num;
    @TableField("tab_value")
    private String table_value;
    private Integer type;
    private String validator;
    private Integer can_delete;
    @TableLogic(value = "0", delval = "1")
    private Integer del_flag;
}
