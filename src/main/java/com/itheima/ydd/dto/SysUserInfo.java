package com.itheima.ydd.dto;

import com.itheima.ydd.entity.User;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class SysUserInfo {
    private String id;
    private String login_name;
    private String password;
    private String name;
    private String email;
    private String phone;
    private String login_ip;
    private LocalDateTime login_date;
    private LocalDateTime create_date;
    private LocalDateTime update_date;
    private Integer status;
    private String default_data;
    private String del_flag;


    private List<String> role_id;
    private List<String> rolename;
}
