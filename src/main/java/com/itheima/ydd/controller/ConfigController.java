package com.itheima.ydd.controller;

import com.itheima.ydd.common.ResultData;
import com.itheima.ydd.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author yan.zhou
 * @date 2022/2/16
 */
@RestController
@RequestMapping("sys_config")
public class ConfigController {
    /**
     * update   POST    已测
     * all      GET     已测
     */
    @Autowired
    private ConfigService configService;

    @GetMapping("all")
    public ResultData all() {

        Map<String, Object> map = configService.all();
        return ResultData.ok("", map);
    }
}