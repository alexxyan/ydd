package com.itheima.ydd.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author yan.zhou
 * @date 2022/2/5
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageResult implements Serializable {
    //总条数
    private Long counts;
    //当前页数
    private Long page;
    //页大小
    private Long pageSize;
    //总页数
    private Long pages;

    private List list;

    /**
     * @param counts      总记录数
     * @param pageSize    每页显示条数
     * @param currentPage // 当前页
     * @param data        // 数据
     * @return
     */
    public static PageResult init(Long counts, Long pageSize, Long currentPage, List data) {
        long totalPage = counts % pageSize == 0 ? counts / pageSize : counts / pageSize + 1;

        return new PageResult(counts, currentPage, pageSize, totalPage, data);
    }
}
