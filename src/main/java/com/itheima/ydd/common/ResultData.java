package com.itheima.ydd.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yan.zhou
 * @date 2022/1/22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultData {
    private Integer errno;
    private String errmsg;
    private Object data;

    public static ResultData ok(String errmsg, Object data) {
        return new ResultData(0, errmsg, data);
    }

    public static ResultData error(String errmsg) {
        return new ResultData(1000, errmsg, null);
    }

    public static ResultData notLogin() {
        return new ResultData(9998, "未登录，不允许操作！", null);
    }
}
