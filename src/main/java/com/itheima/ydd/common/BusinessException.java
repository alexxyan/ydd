package com.itheima.ydd.common;

/**
 * @author yan.zhou
 * @date 2022/1/23
 */
public class BusinessException extends RuntimeException {

    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }
}
