package com.itheima.ydd.common;

/**
 * @author yan.zhou
 * @date 2022/2/11
 */
public class LoginException extends RuntimeException {
    public LoginException() {
    }

    public LoginException(String message) {
        super(message);
    }
}
