package com.itheima.ydd.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

/**
 * @author yan.zhou
 * @date 2022/1/23
 */

@Slf4j
@RestControllerAdvice
public class ControllerAdvice {
    @ExceptionHandler(LoginException.class)
    public ResultData loginExceptionCatcher(LoginException loginException) {
        return ResultData.notLogin();
    }

    @ExceptionHandler(BusinessException.class)
    public ResultData businessExceptionCatcher(BusinessException businessException) {
        log.info(businessException.getMessage());
        return ResultData.error(businessException.getMessage());
    }

    @ExceptionHandler(IOException.class)
    public ResultData ioExceptionCatcher(IOException ioException) {
        ioException.printStackTrace();
        return ResultData.error("找不到对应文件");
    }

    @ExceptionHandler(Exception.class)
    public ResultData exceptionCatcher(Exception exception) {
        exception.printStackTrace();
        return ResultData.error("系统繁忙中，请稍后再试");
    }
}
