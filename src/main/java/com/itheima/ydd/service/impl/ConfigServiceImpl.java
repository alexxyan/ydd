package com.itheima.ydd.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.ydd.common.UserHolder;
import com.itheima.ydd.dto.ConfigDto;
import com.itheima.ydd.dto.SysUserInfo;
import com.itheima.ydd.entity.Config;
import com.itheima.ydd.entity.Menu;
import com.itheima.ydd.entity.Role;
import com.itheima.ydd.entity.User;
import com.itheima.ydd.mapper.ConfigMapper;
import com.itheima.ydd.service.*;
import com.itheima.ydd.service.ConfigService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-21
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements ConfigService {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;

    @Autowired
    private ConfigMapper configMapper;

    @Override
    public Map<String, Object> profile() {
        HashMap<String, Object> map = new HashMap<>();
        //1. 查询用户信息
        String userId = UserHolder.getUserId();
        User user = userService.getById(UserHolder.getUserId());
        SysUserInfo userInfo = new SysUserInfo();
        BeanUtils.copyProperties(user, userInfo);

        //查询用户角色id集合: role_id
        String[] rowIds = user.getRole_id().replace("[", "")
                .replace("]", "")
                .replace("\"", "")
                .split(",");
        List<String> ids = new ArrayList<>();
        Collections.addAll(ids, rowIds);
        userInfo.setRole_id(ids);
        map.put("userInfo", userInfo);

        //查询用户角色名称集合: role_id
        String role_id = UserHolder.get().getRole_id();
        List<String> roleNameList = roleService.roleNameList(role_id);
        userInfo.setRolename(roleNameList);

        //2. 查询角色菜单集合
        List<Menu> menuList = menuService.menuList(role_id);
        map.put("menuList", menuList);


        //region #dictsList 不知道干嘛用的，结果直接为原本数据
        HashMap<String, Object> dictsList = new HashMap<>();
        HashMap<String, String> interface_type = new HashMap<>();
        interface_type.put("get", "get");
        interface_type.put("post", "post");
        dictsList.put("interface_type", interface_type);

        ArrayList<String> table_type = new ArrayList<>();
        table_type.add("单表");
        table_type.add("主表");
        table_type.add("附表");
        table_type.add("树结构表");
        dictsList.put("table_type", table_type);

        ArrayList<String> order_type = new ArrayList<>();
        order_type.add("支付宝");
        order_type.add("微信");
        dictsList.put("order_type", order_type);

        ArrayList<String> report_type = new ArrayList<>();
        report_type.add("正常报告");
        report_type.add("异常报告");
        dictsList.put("report_type", report_type);

        HashMap<String, String> theme = new HashMap<>();
        theme.put("ace", "ACE风格");
        dictsList.put("theme", theme);
        map.put("dictsList", dictsList);

        return map;
    }

    @Override
    public Map<String, Object> all() {
        Map<String, Object> map = new HashMap<>();
        List<Config> configs = configMapper.selectList(new LambdaQueryWrapper<Config>()
                .orderByAsc(Config::getSort_num));

        List<Config> allData = new ArrayList<>();
        Map<String, String> configForm = new HashMap<>();
        Map<String, String> configRules = new HashMap<>();

        List<ConfigDto> dtoList = configs.stream().map((config) -> {
            ConfigDto dto = new ConfigDto();
            BeanUtils.copyProperties(config, dto);

            //处理 allData
            doConfigValue(dto, config);

            // 处理 configForm
            configForm.put(config.getAttr_key(), config.getAttr_value());

            // 处理configRules
            Map<String, String> rules = doConfigRules(config);
            BeanUtils.copyProperties(rules, configRules);

            return dto;
        }).collect(Collectors.toList());


        map.put("allData", dtoList);
        map.put("configForm", configForm);
        map.put("configRules", configRules);
        //TODO
        //map.put("list", null);


        return map;
    }

    private Map<String, String> doConfigRules(Config config) {
        Map<String, String> ruleHashMap = new HashMap<>();
        Integer type = config.getType();
        if (type == 4) {
            String[] rules = config.getConfig_value().split("&");
            for (String rule : rules) {
                String[] split = rule.split("=");
                ruleHashMap.put("label", split[0]);
                ruleHashMap.put("value", split[1]);
            }
        }
        return ruleHashMap;
    }

    /**
     * 处理 ConfigValue
     *
     * @param dto
     * @param config
     * @return
     */
    public ConfigDto doConfigValue(ConfigDto dto, Config config) {

        String config_value = config.getConfig_value();
        if (config_value != null && !"".equals(config_value)) {
            Integer type = config.getType();
            if (type == 4) {
                String[] rules = config_value.split("&");
                List<Map<String, String>> configValue = new ArrayList<>();
                for (String rule : rules) {
                    String[] split = rule.split("=");
                    Map<String, String> ruleHashMap = new HashMap<>();

                    ruleHashMap.put("label", split[0]);
                    ruleHashMap.put("value", split[1]);
                    configValue.add(ruleHashMap);
                }
                dto.setConfig_value(configValue);

                return dto;
            }
        }

        return null;
    }

}
