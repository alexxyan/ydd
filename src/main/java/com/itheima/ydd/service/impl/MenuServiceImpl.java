package com.itheima.ydd.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.ydd.entity.Menu;
import com.itheima.ydd.mapper.MenuMapper;
import com.itheima.ydd.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-22
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> menuList(String role_id) {
        String[] strings = role_id.split(",");
        List<Menu> menuList = menuMapper.findMenuList(strings);
        return menuList;
    }
}
