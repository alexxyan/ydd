package com.itheima.ydd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.ydd.entity.Menu;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-22
 */
public interface MenuService extends IService<Menu> {

    List<Menu> menuList(String role_id);
}
