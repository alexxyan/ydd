package com.itheima.ydd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.ydd.entity.Config;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-21
 */
public interface ConfigService extends IService<Config> {

    Map<String, Object> profile();

    Map<String, Object> all();
}
