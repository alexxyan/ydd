package com.itheima.ydd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.ydd.entity.User;

/**
 * @since 2022-02-18
 */
public interface UserService extends IService<User> {


}
