package com.itheima.ydd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.ydd.entity.Role;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-22
 */
public interface RoleService extends IService<Role> {

    List<String> roleNameList(String role_id);
}
