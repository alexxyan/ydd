package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_config")
public class Config implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 配置名称
     */
    private String group_value;

    /**
     * tab 引入的组件名称
     */
    private String tab_value;

    /**
     * 配置配置键名英文
     */
    private String attr_key;

    /**
     * 配置字段中文名
     */
    private String attr_name;

    /**
     * 配置值内容
     */
    private String attr_value;

    /**
     * 0正常 1 已删除
     */
    private Integer del_flag;

    /**
     * 字段类型，参见字段表
     */
    private Integer type;

    /**
     * 单选多选数据值
     */
    private String config_value;

    /**
     * 验证规则
     */
    private String validator;

    /**
     * 输入框提示文字
     */
    private String placeholder;

    /**
     * 0 不可删除，1可删除
     */
    private Integer can_delete;

    /**
     * 显示权重，数字大的往后
     */
    private Integer sort_num;


}
