package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 登录名
     */
    private String login_name;

    /**
     * 密码
     */
    private String password;

    /**
     * 姓名
     */
    private String name;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 电话
     */
    private String phone;

    /**
     * 最后登陆IP
     */
    private String login_ip;

    /**
     * 最后登陆时间
     */
    private LocalDateTime login_date;

    /**
     * 创建时间
     */
    private LocalDateTime create_date;

    /**
     * 更新时间
     */
    private LocalDateTime update_date;

    /**
     * 删除标记
     */
    private String del_flag;

    /**
     * 角色ID
     */
    private String role_id;

    /**
     * 状态 1 正常  0 冻结
     */
    private Integer status;

    /**
     * 是否为默认数据，null表示不是，1表示是
     */
    private String default_data;


}
