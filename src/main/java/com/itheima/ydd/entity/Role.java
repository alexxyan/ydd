package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author yan.zhou
 * @since 2022-02-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色描述
     */
    private String description;

    /**
     * 创建时间
     */
    private LocalDateTime create_date;

    /**
     * 更新时间
     */
    private LocalDateTime update_date;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    private String del_flag;

    /**
     * 是否为超级管理员
     */
    private Integer superadmin;

    /**
     * 是否为默认数据，null表示不是，1表示是
     */
    private String default_data;


}
