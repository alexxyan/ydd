package com.itheima.test;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * @author boller
 * @creatTime 2022/2/25 15:07
 */
public class test {
    @Test
    public void testRedis() {
        //1 获取连接
        //Jedis jedis = new Jedis("localhost",6379);
        Jedis jedis = new Jedis();

        //2 执行具体的操作
        jedis.set("username", "xiaoming");

        String value = jedis.get("username");
        System.out.println(value);

        //jedis.del("username");

        jedis.hset("myhash", "addr", "bj");
        String hValue = jedis.hget("myhash", "addr");
        System.out.println(hValue);

        Set<String> keys = jedis.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }

        //3 关闭连接
        jedis.close();
    }
}
